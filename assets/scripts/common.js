$(document).ready(function() {
	$('.js-filter-switch').click(function() {
		$('.js-filter-switch').removeClass('active');
		$(this).addClass('active');
		var cat = $(this).data('category');
		$('.category-items li').removeClass('active');
		$('.category-items li[data-item-category="'+cat+'"]').addClass('active');
		
		$('.category-items li.clone').remove();
		var items = $('.category-items li.active');
		if ((items.length % 4 < 3) && (items.length % 4 != 0)){
			var clone = '<li class="clone"><a><img src="assets/images/img01.png"/></a></li>';
			$('.category-items').append(clone.repeat(4 - items.length % 4));
		}
	});
	$('.js-menu-switch').click(function(e) {
		e.preventDefault();
		$('.js-menu-switch').removeClass('active');
		$(this).addClass('active');
		console.log($(this).attr('href').replace('#',''));
		var scrollTo = $('section[data-item-type="'+$(this).attr('href').replace('#','')+'"]');

		setTimeout(function() {
			var body = $("html, body");
			body.stop().animate({scrollTop: scrollTo.offset().top}, 500, 'swing', function() { 
				// alert("Finished animating");
			});
		}, 500);
	});
	$('.js-menu-toggle').click(function() {
		$('.header__menu').toggleClass('active');
	});
	$('.js-footer-switch').click(function() {
		$('.js-footer-switch').removeClass('active');
		$(this).addClass('active');
	});

})